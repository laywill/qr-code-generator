# See https://pypi.org/project/qrcode/
import qrcode

# Quick and easy method
img = qrcode.make('https://www.linkedin.com/in/laywill/')
type(img)  # qrcode.image.pil.PilImage
img.save("some_file.png")

# Invoke the class and customise it further
qr = qrcode.QRCode(
    version=None,
    error_correction=qrcode.constants.ERROR_CORRECT_M,
    box_size=10,
    border=4,
)
qr.add_data('https://www.linkedin.com/in/laywill/')
qr.make(fit=True)

img_adv = qr.make_image(fill_color="black", back_color="white")
type(img)  # qrcode.image.pil.PilImage
img_adv.save("some_file_adv.png")
